//
//  MainViewController.swift
//  CreditCardValidator
//
//  Created by Vladimir. Evstratov on 12/24/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit
import CreditCardValidatorFramework

class MainViewController: UIViewController {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var numberTextField: UITextField!
    @IBOutlet weak var validationResult: UILabel!
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var schemeLogo: UIImageView!
    @IBOutlet weak var countryFlag: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    private let validator = CardEvaluator()
    private let invalidCardNumberEmoji = "☹️"
    private let validCardNumberEmoji = "😀"
    private var isValid: Bool? {
        didSet {
            if let value = isValid {
                validationResult.text = value ? validCardNumberEmoji : invalidCardNumberEmoji
            } else {
                validationResult.text = nil
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        validator.delegate = self
    }
    
    private func setupViews() {
        cardView.layer.cornerRadius = 15.0
        cardView.layer.borderWidth = 1.0
        cardView.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        numberTextField.keyboardType = .numberPad
        numberTextField.addTarget(self, action: #selector(textChanged), for: .editingChanged)
        bankLabel.text = nil
        countryFlag.text = nil
        messageLabel.text = nil
        isValid = nil
    }
    
    @objc private func textChanged() {
        validator.cardNumber = numberTextField.text
    }    
}

extension MainViewController: CardEvaluatorDelegate {
    func cardDetailsDidUpdated(_ details: PaymentCardModel?, _ error: Error?) {
        bankLabel.text = details?.bank?.name
        countryFlag.text = details?.country?.countryFlag
        
        var logo: UIImage?
        switch details?.scheme {
        case "visa":
            logo = UIImage(named: "visa-logo")
        case "mastercard":
            logo = UIImage(named: "MasterCard-logo")
        default:
            break
        }
        schemeLogo.image = logo
    }
    
    func cardNumberDidValidated(_ result: CardEvaluationResult, _ error: Error?) {
        switch result {
        case .valid:
            isValid = true
        case .invalid:
            isValid = false
        case .undefined:
            isValid = nil
        }
        messageLabel.text = error?.localizedDescription
    }    
}
